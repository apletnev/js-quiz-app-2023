
const questionParent = document.querySelector(".question-container");
const optionParent   = document.querySelector(".option-container");
const nextButton     = document.querySelector(".next");
const quitButton     = document.querySelector(".quit");
const startTheTestButton = document.querySelector(".start-the-test");
const quizCategory   = document.querySelector(".quiz-category");
const qsnCount       = document.querySelector(".qsn-count");
const curScore       = document.querySelector(".cur-score");
const quizParent     = document.querySelector(".quiz");
const mainpage       = document.querySelector(".main-page");

let quizzes = [];
let currentQuestionIndex = 0;
let score = 0;

const URL = "https://opentdb.com/api.php?amount=5&category=9&difficulty=easy&type=multiple";

const getData = async (URL) => {
    try{
        const {data : {results}} = await axios.get(URL);
        return results;

    }catch(err){
        console.log('error:', err);
    }
}

const getQuizzes = async () => {
    quizzes = await getData(URL);
    //console.log(quizzes);
}

quizParent.style.display = "none";

function createQuestionAndOption(quizzes, index){
    
    if (quizzes.length === index) {
        //last question has been
        quizCategory.innerText = "";
        curScore.innerText = `Your score: ${score}`;
        nextButton.style.display = "none";

    } else {
        quizCategory.innerText = `${quizzes[index].category}`;
        qsnCount.innerText = `Q${index+1}/${quizzes.length}`;

        const questionElement = document.createElement("p");
        questionElement.innerText = quizzes[index].question;
        questionParent.appendChild(questionElement);
        let options = [quizzes[index].correct_answer, ...quizzes[index].incorrect_answers].sort(()=> Math.random() - 0.5);
        for (let option of options){
            const optionBtn = document.createElement("button");
            optionBtn.setAttribute("name", option);
            optionBtn.classList.add("button");
            optionBtn.innerText = option;
            optionParent.appendChild(optionBtn);
        }
    }
}

function disableOption(){
    document
        .querySelectorAll(".button")
        .forEach((button) => (button.disabled = true));
}

optionParent.addEventListener("click", (ev) => {
    if (ev.target.name === quizzes[currentQuestionIndex].correct_answer){
        ev.target.classList.add("correct");
        score++;
        curScore.innerText = `Score: ${score}`;
        disableOption();
    } else if (ev.target.name !== quizzes[currentQuestionIndex].correct_answer) {
        ev.target.classList.add("incorrect");
        disableOption();
    }
})

nextButton.addEventListener("click", () => {
    currentQuestionIndex++;
    questionParent.innerHTML = "";
    optionParent.innerHTML   = "";
    createQuestionAndOption(quizzes, currentQuestionIndex);
})

startTheTestButton.addEventListener("click", (ev) => {
    mainpage.style.display   = "none";
    quizParent.style.display = "block";
    questionParent.innerHTML = "";
    optionParent.innerHTML   = ""; 
    
    quizzes = [];
    currentQuestionIndex = 0;
    score = 0;   
    curScore.innerText = `Score: ${score}`;

    getQuizzes();
    setTimeout(() => createQuestionAndOption(quizzes, currentQuestionIndex), 2000);
})

quitButton.addEventListener("click", (ev) => {
    questionParent.innerHTML = "";
    optionParent.innerHTML   = "";
    mainpage.style.display   = "block";
    quizParent.style.display = "none";
    quizzes = [];
    currentQuestionIndex = 0;
    score = 0; 
    curScore.innerText = `Score: ${score}`; 
    if (nextButton.style.display === "none"){
        nextButton.style.display = "inline-block";
    }
})